Robert Ronning is an entrepreneur and financial technology professional from British Columbia. He has developed his comprehensive knowledge of the payment processing industry, financial technologies and the challenges and opportunities that face merchants for the last decade as a business development professional, a senior account executive and as a co-founder of PayVida Solutions. He is singularly minded in his commitment to taking existing multi-channel payment technologies and services and develop them into next generation mechanisms to deliver value to customers. He has assembled a team of product and commercialization professionals to achieve this goal. Most seriously, Robert is committed to closing the ethics gap that plagues the Canadian Payment industry. 

Robert, as a co-founder of PayVida, is currently leading all strategic business development aspects of the company. He provides instrumental direction and ideas on current and upcoming payment technology and integration, growth of enterprise relationships, providing expertise to inside and outside sales teams responsible for commercialization, and PayVidas global go-to-market activities .

Aside from PayVida, Robert is committed to entrepreneurship, start-ups and assisting the trailblazers looking to make their own mark in the business world and community involvement.

Address:  1350 Burrard Street, Suite 182, Vancouver, BC V6Z 0C2

Phone: 778-714-0736
